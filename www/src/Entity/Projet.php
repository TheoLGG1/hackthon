<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $client;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeProjet", inversedBy="projets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeProjet;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $budget;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $deadline;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="projets")
     */
    private $equipe;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Metier", inversedBy="projets")
     */
    private $equipe_necessaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="my_projects")
     */
    private $chef_projet;


    public function __construct()
    {
        $this->equipe = new ArrayCollection();
        $this->equipe_necessaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTypeProjet(): ?TypeProjet
    {
        return $this->typeProjet;
    }

    public function setTypeProjet(?TypeProjet $typeProjet): self
    {
        $this->typeProjet = $typeProjet;

        return $this;
    }

    public function getBudget(): ?float
    {
        return $this->budget;
    }

    public function setBudget(?float $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getDeadline(): ?\DateTimeInterface
    {
        return $this->deadline;
    }

    public function setDeadline(?\DateTimeInterface $deadline): self
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEquipe(): Collection
    {
        return $this->equipe;
    }

    public function addEquipe(User $equipe): self
    {
        if (!$this->equipe->contains($equipe)) {
            $this->equipe[] = $equipe;
        }

        return $this;
    }

    public function removeEquipe(User $equipe): self
    {
        if ($this->equipe->contains($equipe)) {
            $this->equipe->removeElement($equipe);
        }

        return $this;
    }

    /**
     * @return Collection|Metier[]
     */
    public function getEquipeNecessaire(): Collection
    {
        return $this->equipe_necessaire;
    }

    public function addEquipeNecessaire(Metier $equipeNecessaire): self
    {
        if (!$this->equipe_necessaire->contains($equipeNecessaire)) {
            $this->equipe_necessaire[] = $equipeNecessaire;
        }

        return $this;
    }

    public function removeEquipeNecessaire(Metier $equipeNecessaire): self
    {
        if ($this->equipe_necessaire->contains($equipeNecessaire)) {
            $this->equipe_necessaire->removeElement($equipeNecessaire);
        }

        return $this;
    }

    public function getChefProjet(): ?User
    {
        return $this->chef_projet;
    }

    public function setChefProjet(?User $chef_projet): self
    {
        $this->chef_projet = $chef_projet;

        return $this;
    }

}
