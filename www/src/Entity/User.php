<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user_account")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToOne(targetEntity="Experience")
     * @ORM\JoinColumn(nullable=true)
     */
    private $experience;

    /**
     * @ORM\ManyToOne(targetEntity="Metier")
     * @ORM\JoinColumn(nullable=true)
     */
    private $metier;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="boolean")
     */
    private $agreeTerms;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserFormation", mappedBy="user_id")
     */
    private $userFormations;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Projet", mappedBy="equipe")
     */
    private $projets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="chef_projet")
     */
    private $my_projects;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tauxJournalier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Niveau", mappedBy="acquired_by")
     */
    private $niveaux;

    public function __construct()
    {
        $this->niveaux = new ArrayCollection();
        $this->userFormations = new ArrayCollection();
        $this->projets = new ArrayCollection();
        $this->my_projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getExperience(): ?Experience
    {
        return $this->experience;
    }

    public function setExperience(?Experience $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getMetier(): ?Metier
    {
        return $this->metier;
    }

    public function setMetier(?Metier $metier): self
    {
        $this->metier = $metier;

        return $this;
    }

    public function setPlainPassword(string $password): self
    {
        $this->plainPassword = $password;
        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function getAgreeTerms(): ?bool
    {
        return $this->agreeTerms;
    }

    public function setAgreeTerms(bool $agreeTerms): self
    {
        $this->agreeTerms = $agreeTerms;

        return $this;
    }

    /**
     * @return Collection|UserFormation[]
     */
    public function getUserFormations(): Collection
    {
        return $this->userFormations;
    }

    public function addUserFormation(UserFormation $userFormation): self
    {
        if (!$this->userFormations->contains($userFormation)) {
            $this->userFormations[] = $userFormation;
            $userFormation->setUserId($this);
        }

        return $this;
    }

    public function removeUserFormation(UserFormation $userFormation): self
    {
        if ($this->userFormations->contains($userFormation)) {
            $this->userFormations->removeElement($userFormation);
            // set the owning side to null (unless already changed)
            if ($userFormation->getUserId() === $this) {
                $userFormation->setUserId(null);
            }
        }

        return $this;
    }

    public function isAdmin(): bool
    {
        foreach ($this->roles as $key => $value){
            if($value == "ROLE_ADMIN") {
                return true;
            }
        }
        return false;
    }

    public function __toString()
    {
        return $this->firstname . " " . $this->lastname;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjets(): Collection
    {
        return $this->projets;
    }

    public function addProjet(Projet $projet): self
    {
        if (!$this->projets->contains($projet)) {
            $this->projets[] = $projet;
            $projet->addEquipe($this);
        }

        return $this;
    }

    public function removeProjet(Projet $projet): self
    {
        if ($this->projets->contains($projet)) {
            $this->projets->removeElement($projet);
            $projet->removeEquipe($this);
        }

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getMyProjects(): Collection
    {
        return $this->my_projects;
    }

    public function addMyProject(Projet $myProject): self
    {
        if (!$this->my_projects->contains($myProject)) {
            $this->my_projects[] = $myProject;
            $myProject->setChefProjet($this);
        }

        return $this;
    }

    public function removeMyProject(Projet $myProject): self
    {
        if ($this->my_projects->contains($myProject)) {
            $this->my_projects->removeElement($myProject);
            // set the owning side to null (unless already changed)
            if ($myProject->getChefProjet() === $this) {
                $myProject->setChefProjet(null);
            }
        }

        return $this;
    }

    public function getTauxJournalier(): ?float
    {
        return $this->tauxJournalier;
    }

    public function setTauxJournalier(?float $tauxJournalier): self
    {
        $this->tauxJournalier = $tauxJournalier;

        return $this;
    }

    public function isChefProjet(): bool
    {
        return $this->metier->getCode() == "chef_projet";
    }

    /**
     * @return Collection|Niveau[]
     */
    public function getNiveaux(): Collection
    {
        return $this->niveaux;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveaux->contains($niveau)) {
            $this->niveaux[] = $niveau;
            $niveau->setAcquiredBy($this);
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        if ($this->niveaux->contains($niveau)) {
            $this->niveaux->removeElement($niveau);
            // set the owning side to null (unless already changed)
            if ($niveau->getAcquiredBy() === $this) {
                $niveau->setAcquiredBy(null);
            }
        }

        return $this;
    }

    public function isDisponible($value): bool
    {
        if($this->projets->count() != 0) {
            foreach ($this->projets as $projet) {
                if ($projet->getId() == $value->getId()) {
                    return false;
                }
            }
        }
        return true;
    }
}
