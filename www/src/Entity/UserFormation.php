<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserFormationRepository")
 */
class UserFormation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userFormations")
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="userFormations")
     */
    private $formation_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EtatFormation", inversedBy="userFormations")
     */
    private $etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getFormationId(): ?Formation
    {
        return $this->formation_id;
    }

    public function setFormationId(?Formation $formation_id): self
    {
        $this->formation_id = $formation_id;

        return $this;
    }

    public function getEtat(): ?EtatFormation
    {
        return $this->etat;
    }

    public function setEtat(?EtatFormation $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
