<?php

namespace App\Repository;

use App\Entity\Projet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Projet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projet[]    findAll()
 * @method Projet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Projet::class);
    }

    public function findByTypeProjet($value)
    {
        return $this->createQueryBuilder('p')
                    ->leftJoin('p.typeProjet', 'tp')
                    ->andWhere('tp.id = :id')
                    ->setParameter('id',$value)
                    ->getQuery()
                    ->getResult();
    }

    public function findDisponible()
    {
        return $this->createQueryBuilder('p')
                    ->andWhere('p.chef_projet is NULL')
                    ->getQuery()
                    ->getResult();
    }

    public function findNonDisponible()
    {
        return $this->createQueryBuilder('p')
                    ->andWhere('p.chef_projet is not NULL')
                    ->getQuery()
                    ->getResult();
    }

    public function findByUser($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere(':user in (:equipe)')
            ->setParameter('equipe','p.equipe')
            ->setParameter('user',$value)
            ->getQuery()
            ->getResult();
    }

    public function findByChefProjet($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.chef_projet = :user')
            ->setParameter('user',$value)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Projet[] Returns an array of Projet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Projet
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
