<?php

namespace App\Controller\Back;

use App\Entity\Projet;
use App\Entity\TypeProjet;
use App\Entity\User;
use App\Form\ProjetType;
use App\Repository\MetierRepository;
use App\Repository\ProjetRepository;
use App\Repository\TypeProjetRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/projet")
 */
class ProjetController extends AbstractController
{
    /**
     * @Route("/", name="projet_index", methods={"GET"})
     */
    public function index(ProjetRepository $projetRepository, TypeProjetRepository $typeProjetRepository): Response
    {
        return $this->render('projet/index.html.twig', [
            'projets_user' => $projetRepository->findAll(),
            'typeProjets' => $typeProjetRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="projet_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $projet = new Projet();
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($projet);
            $entityManager->flush();

            return $this->redirectToRoute('admin_projet_index');
        }

        return $this->render('projet/new.html.twig', [
            'projet' => $projet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="projet_show", methods={"GET"})
     */
    public function show(Projet $projet, UserRepository $userRepository,ProjetRepository $projetRepository, TypeProjetRepository $typeProjetRepository): Response
    {


        return $this->render('projet/show.html.twig', [
            'projet' => $projet,
            'chef_projets' => $userRepository->findByMetier('chef_projet'),
            'projets' => $projetRepository->findAll(),
            'typeProjets' => $typeProjetRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="projet_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Projet $projet): Response
    {
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_projet_index');
        }

        return $this->render('projet/edit.html.twig', [
            'projet' => $projet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="projet_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Projet $projet): Response
    {
        if ($this->isCsrfTokenValid('delete'.$projet->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($projet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_projet_index');
    }

    /**
     * @Route("/{id}/affect/{user_id}", name="projet_affect_chef", methods={"GET"})
     *
     * @param Request $request
     */
    public function affectChefProjet(Request $request, Projet $projet, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['id' => $request->get('user_id')]);
        $projet->setChefProjet($user);
        $user->addMyProject($projet);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute('admin_projet_show', ['id' => $projet->getId()]);
    }

    /**
     * @Route("?filter['metier']={id}", name="projet_filter_type", methods={"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function filterProjectsByType(Request $request, Projet $projet, ProjetRepository $projetRepository, TypeProjetRepository $typeProjetRepository): Response
    {
        $projets = $projetRepository->findByTypeProjet($request->get('id'));

        return $this->render('projet/index.html.twig', [
            'projets_user' => $projets,
            'typeProjets' => $typeProjetRepository->findAll()
        ]);
    }

    /**
     * @Route("?filter['status']={status}", name="projet_filter_status", methods={"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function filterProjetsByStatus(Request $request, ProjetRepository $projetRepository, TypeProjetRepository $typeProjetRepository): Response
    {
        $status = $request->get('status');
        $projets = [];
        if ($status == "disponible") {
            $projets = $projetRepository->findDisponible();
            return $this->render('projet/indexDispo.html.twig', [
                'projets_user' => $projets,
                'typeProjets' => $typeProjetRepository->findAll()
            ]);
        }
        else {
            $projets = $projetRepository->findNonDisponible();
            return $this->render('projet/indexNonDispo.html.twig', [
                'projets_user' => $projets,
                'typeProjets' => $typeProjetRepository->findAll()
            ]);
        }


    }
}
