<?php


namespace App\Controller\Front;


use App\Entity\EtatFormation;
use App\Entity\Formation;
use App\Entity\User;
use App\Entity\UserFormation;
use App\Form\FormationType;
use App\Repository\CompetenceRepository;
use App\Repository\EtatFormationRepository;
use App\Repository\FormationRepository;
use App\Repository\UserFormationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("user/formation")
 */
class FormationController extends AbstractController
{

    /**
     * @Route("/", name="formation_index", methods={"GET"})
     */
    public function index(FormationRepository $formationRepository,CompetenceRepository $competenceRepository): Response
    {
        return $this->render('back_freelancer/formation/index.html.twig', [
            'formations' => $formationRepository->findAll(),
            'competences' => $competenceRepository->findAll()

        ]);
    }

    /**
     * @Route("/subscribed", name="formation_subscribed", methods={"GET"})
     */
    public function subscribed(FormationRepository $formationRepository):Response
    {
        return $this->render('back_freelancer/formation/subscribedshow.html.twig', [
            'formations' => $formationRepository->findWhereNotUser($this->getUser()),
        ]);
    }

    /**
     * @Route("/{id}", name="formation_show", methods={"GET","POST"})
     */
    public function show(Formation $formation,FormationRepository $formationRepository): Response
    {
        return $this->render('back_freelancer/formation/show.html.twig', [
            'formation' => $formation,
            'formations' => $formationRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}/subs/", name="formation_subscribe", methods={"GET","POST"})
     */
    public function subscribe(Formation $formation, EtatFormationRepository $etatFormation, Request $request){
        $subscribe = new UserFormation();
        $subscribe->setUserId($this->getUser());
        $subscribe->setFormationId($formation);
        $subscribe->setEtat($etatFormation->findOneBy(['code'=>'C']));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($subscribe);
        $entityManager->flush();
        return $this->redirectToRoute('formation_index');
    }

//    /**
//     * @Route("/{id}/edit", name="formation_edit", methods={"GET","POST"})
//     */
//    public function edit(Request $request, Formation $formation): Response
//    {
//        $form = $this->createForm(FormationType::class, $formation);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('formation_index');
//        }
//
//        return $this->render('back_freelancer/formation/edit.html.twig', [
//            'formation' => $formation,
//            'form' => $form->createView(),
//        ]);
//    }

//    /**
//     * @Route("/{id}", name="formation_delete", methods={"DELETE"})
//     */
//    public function delete(Request $request, Formation $formation): Response
//    {
//        if ($this->isCsrfTokenValid('delete'.$formation->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($formation);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('formation_index');
//    }
}