<?php


namespace App\Controller\Front;


use App\Entity\Competence;
use App\Entity\Metier;
use App\Entity\Niveau;
use App\Entity\Projet;
use App\Form\UserAccountType;
use App\Repository\CompetenceRepository;
use App\Repository\MetierRepository;
use App\Repository\ProjetRepository;
use App\Repository\TypeProjetRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/profile", name="user_profile", methods={"GET","POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function userAccount(Request $request){
        $form = $this->createForm(UserAccountType::class, $this->getUser());

        return $this->render('user/edit.html.twig', [
            'accountForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/missions", name="user_missions", methods={"GET"})
     *
     * @param ProjetRepository $projetRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function missionIndex(ProjetRepository $projetRepository){
        return $this->render('projet/index.html.twig', [
            'projets_user' => $projetRepository->findByUser($this->getUser()),
            'projets_chef' => $projetRepository->findByChefProjet($this->getUser())
        ]);
    }

    /**
     * @Route("/mission/{id}", name="user_show_mission", methods={"GET"})
     */
    public function missionShow(Projet $projet, UserRepository $userRepository,ProjetRepository $projetRepository, TypeProjetRepository $typeProjetRepository): Response
    {
        return $this->render('projet/selectEquipe.html.twig', [
            'projet' => $projet,
            'user_disponible' => $userRepository->findDisponible($projet),
            'projets' => $projetRepository->findAll(),
            'typeProjets' => $typeProjetRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}/select/{user_id}", name="user_select_equipe", methods={"GET"})
     *
     * @param Request $request
     */
    public function affectChefProjet(Request $request, Projet $projet, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['id' => $request->get('user_id')]);
        $projet->addEquipe($user);
        $user->addProjet($projet);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return $this->redirectToRoute('user_show_mission', ['id' => $projet->getId()]);
    }

    /**
     * @Route("/{id}/competences", name="user_competences", methods={"GET"})
     */
    public function userCompetence(CompetenceRepository $competenceRepository, MetierRepository $metierRepository)
    {
        return $this->render('competence/userCompetences.html.twig', [
            'competences' => $competenceRepository->findAll(),
            'metiers' => $metierRepository->findAll()
        ]);
    }

    /**
     * @Route("?filter['metier']={id}", name="competence_filter_metier", methods={"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function filterCompetencesByType(Request $request, Projet $projet, CompetenceRepository $competenceRepository, MetierRepository $metierRepository): Response
    {
        $competences = $competenceRepository->findByMetier($request->get('id'));

        return $this->render('competence/userCompetences.html.twig', [
            'competences' => $competences,
            'metiers' => $metierRepository->findAll()
        ]);
    }

    /**
     * @Route("/{id}/acquerir/{competence_id}", name="user_acquerir_competence", methods={"GET"})
     */
    public function userAcquerirCompetence(Request $request, Competence $competence, CompetenceRepository $competenceRepository, MetierRepository $metierRepository): Response
    {
        $competence = $competenceRepository->findOneBy(['id' => $request->get('competence_id')]);

        $niveau = new Niveau();
        $niveau->setAcquiredBy($this->getUser());
        $niveau->setCompetence($competence);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($niveau);
        $entityManager->flush();

        return $this->redirectToRoute('user_competences', ['id' => $this->getUser()->getId()]);
    }
}