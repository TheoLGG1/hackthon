<?php

namespace App\Form;

use App\Entity\Projet;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class ProjetType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //dd($this->security->getUser());

        $builder
            ->add('title')
            ->add('client')
            ->add('description');
            if($this->security->getUser()->isAdmin()){
                $builder->add('chef_projet');
            } else {
                $builder->add('equipe');
            }
        $builder->add('typeProjet')
                ->add('budget')
                ->add('deadline', DateType::class)
                ->add('equipe_necessaire');
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Projet::class,
        ]);
    }
}
