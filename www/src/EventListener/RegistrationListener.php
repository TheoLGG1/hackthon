<?php


namespace App\EventListener;

use App\Entity\User;
use App\Repository\MetierRepository;
use App\Repository\ExperienceRepository;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationListener
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    public function prePersist(User $user, LifecycleEventArgs $args)
    {
        $user = $args->getObject();
        if ($user->getPlainPassword() != null) {
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $user->getPlainPassword()
            ));
            $user->setAgreeTerms(true);

        }
    }
}