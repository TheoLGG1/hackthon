<?php


namespace App\EventListener;

use App\Entity\Projet;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class AddChefProjetListener
{
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    public function prePersist(Projet $projet, LifecycleEventArgs $args)
    {

    }
}