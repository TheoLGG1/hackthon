<?php


namespace App\Service;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MailerService extends AbstractController
{
    /**
     * @param $to
     * @param $subject
     * @param $body_text
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendMail($mailer, $to, $subject, $mail_template, $user){

        $email = (new TemplatedEmail())
            ->from('adwhackthon@gmail.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($mail_template)
            ->context([
                'user' => $user,
                'token' => $user->getConfirmationToken(),
            ]);

        $mailer->send($email);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}