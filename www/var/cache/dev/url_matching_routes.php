<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin/competence' => [[['_route' => 'admin_competence_index', '_controller' => 'App\\Controller\\Back\\CompetenceController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/competence/new' => [[['_route' => 'admin_competence_new', '_controller' => 'App\\Controller\\Back\\CompetenceController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/experience' => [[['_route' => 'admin_experience_index', '_controller' => 'App\\Controller\\Back\\ExperienceController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/experience/new' => [[['_route' => 'admin_experience_new', '_controller' => 'App\\Controller\\Back\\ExperienceController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/formation' => [[['_route' => 'admin_formation_index', '_controller' => 'App\\Controller\\Back\\FormationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/formation/new' => [[['_route' => 'admin_formation_new', '_controller' => 'App\\Controller\\Back\\FormationController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin' => [[['_route' => 'admin_index', '_controller' => 'App\\Controller\\Back\\HomeController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/metier' => [[['_route' => 'admin_metier_index', '_controller' => 'App\\Controller\\Back\\MetierController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/metier/new' => [[['_route' => 'admin_metier_new', '_controller' => 'App\\Controller\\Back\\MetierController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/projet' => [[['_route' => 'admin_projet_index', '_controller' => 'App\\Controller\\Back\\ProjetController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/projet/new' => [[['_route' => 'admin_projet_new', '_controller' => 'App\\Controller\\Back\\ProjetController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/user/formation' => [[['_route' => 'formation_index', '_controller' => 'App\\Controller\\Front\\FormationController::index'], null, ['GET' => 0], null, true, false, null]],
        '/user/formation/subscribed' => [[['_route' => 'formation_subscribed', '_controller' => 'App\\Controller\\Front\\FormationController::subscribed'], null, ['GET' => 0], null, false, false, null]],
        '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\Front\\HomeController::index'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/user' => [[['_route' => 'user_index', '_controller' => 'App\\Controller\\Front\\HomeController::user_index'], null, ['GET' => 0], null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\Front\\RegistrationController::register'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\Front\\SecurityController::login'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\Front\\SecurityController::logout'], null, null, null, false, false, null]],
        '/user/profile' => [[['_route' => 'user_profile', '_controller' => 'App\\Controller\\Front\\UserController::userAccount'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/user/missions' => [[['_route' => 'user_missions', '_controller' => 'App\\Controller\\Front\\UserController::missionIndex'], null, ['GET' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/admin/(?'
                    .'|competence/([^/]++)(?'
                        .'|(*:201)'
                        .'|/edit(*:214)'
                        .'|(*:222)'
                    .')'
                    .'|experience/([^/]++)(?'
                        .'|(*:253)'
                        .'|/edit(*:266)'
                        .'|(*:274)'
                    .')'
                    .'|formation/([^/]++)(?'
                        .'|(*:304)'
                        .'|/edit(*:317)'
                        .'|(*:325)'
                    .')'
                    .'|metier/([^/]++)(?'
                        .'|(*:352)'
                        .'|/edit(*:365)'
                        .'|(*:373)'
                    .')'
                    .'|projet(?'
                        .'|/([^/]++)(?'
                            .'|(*:403)'
                            .'|/(?'
                                .'|edit(*:419)'
                                .'|affect/([^/]++)(*:442)'
                            .')'
                            .'|(*:451)'
                        .')'
                        .'|\\?filter\\[\'(?'
                            .'|metier\'\\]\\=([^/]++)(*:493)'
                            .'|status\'\\]\\=([^/]++)(*:520)'
                        .')'
                    .')'
                .')'
                .'|/user(?'
                    .'|/(?'
                        .'|formation/([^/]++)(?'
                            .'|(*:564)'
                            .'|/subs(*:577)'
                        .')'
                        .'|mission/([^/]++)(*:602)'
                        .'|([^/]++)/(?'
                            .'|select/([^/]++)(*:637)'
                            .'|competences(*:656)'
                            .'|acquerir/([^/]++)(*:681)'
                        .')'
                    .')'
                    .'|\\?filter\\[\'metier\'\\]\\=([^/]++)(*:721)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        201 => [[['_route' => 'admin_competence_show', '_controller' => 'App\\Controller\\Back\\CompetenceController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        214 => [[['_route' => 'admin_competence_edit', '_controller' => 'App\\Controller\\Back\\CompetenceController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        222 => [[['_route' => 'admin_competence_delete', '_controller' => 'App\\Controller\\Back\\CompetenceController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        253 => [[['_route' => 'admin_experience_show', '_controller' => 'App\\Controller\\Back\\ExperienceController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        266 => [[['_route' => 'admin_experience_edit', '_controller' => 'App\\Controller\\Back\\ExperienceController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        274 => [[['_route' => 'admin_experience_delete', '_controller' => 'App\\Controller\\Back\\ExperienceController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        304 => [[['_route' => 'admin_formation_show', '_controller' => 'App\\Controller\\Back\\FormationController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        317 => [[['_route' => 'admin_formation_edit', '_controller' => 'App\\Controller\\Back\\FormationController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        325 => [[['_route' => 'admin_formation_delete', '_controller' => 'App\\Controller\\Back\\FormationController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        352 => [[['_route' => 'admin_metier_show', '_controller' => 'App\\Controller\\Back\\MetierController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        365 => [[['_route' => 'admin_metier_edit', '_controller' => 'App\\Controller\\Back\\MetierController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        373 => [[['_route' => 'admin_metier_delete', '_controller' => 'App\\Controller\\Back\\MetierController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        403 => [[['_route' => 'admin_projet_show', '_controller' => 'App\\Controller\\Back\\ProjetController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        419 => [[['_route' => 'admin_projet_edit', '_controller' => 'App\\Controller\\Back\\ProjetController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        442 => [[['_route' => 'admin_projet_affect_chef', '_controller' => 'App\\Controller\\Back\\ProjetController::affectChefProjet'], ['id', 'user_id'], ['GET' => 0], null, false, true, null]],
        451 => [[['_route' => 'admin_projet_delete', '_controller' => 'App\\Controller\\Back\\ProjetController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        493 => [[['_route' => 'admin_projet_filter_type', '_controller' => 'App\\Controller\\Back\\ProjetController::filterProjectsByType'], ['id'], ['GET' => 0], null, false, true, null]],
        520 => [[['_route' => 'admin_projet_filter_status', '_controller' => 'App\\Controller\\Back\\ProjetController::filterProjetsByStatus'], ['status'], ['GET' => 0], null, false, true, null]],
        564 => [[['_route' => 'formation_show', '_controller' => 'App\\Controller\\Front\\FormationController::show'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        577 => [[['_route' => 'formation_subscribe', '_controller' => 'App\\Controller\\Front\\FormationController::subscribe'], ['id'], ['GET' => 0, 'POST' => 1], null, true, false, null]],
        602 => [[['_route' => 'user_show_mission', '_controller' => 'App\\Controller\\Front\\UserController::missionShow'], ['id'], ['GET' => 0], null, false, true, null]],
        637 => [[['_route' => 'user_select_equipe', '_controller' => 'App\\Controller\\Front\\UserController::affectChefProjet'], ['id', 'user_id'], ['GET' => 0], null, false, true, null]],
        656 => [[['_route' => 'user_competences', '_controller' => 'App\\Controller\\Front\\UserController::userCompetence'], ['id'], ['GET' => 0], null, false, false, null]],
        681 => [[['_route' => 'user_acquerir_competence', '_controller' => 'App\\Controller\\Front\\UserController::userAcquerirCompetence'], ['id', 'competence_id'], ['GET' => 0], null, false, true, null]],
        721 => [
            [['_route' => 'competence_filter_metier', '_controller' => 'App\\Controller\\Front\\UserController::filterCompetencesByType'], ['id'], ['GET' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
